import {Request, Controller, Get, Logger} from '@nestjs/common';
import { PokemonsService } from './pokemons.service';
import _ from 'lodash'
@Controller('pokemons')
export class PokemonsController {
  constructor(private readonly pokemonService: PokemonsService) {}
  private readonly logger = new Logger(PokemonsController.name);
  @Get()
  async getAllPokemons() {
    return this.pokemonService.getPokemons()
  }
}
