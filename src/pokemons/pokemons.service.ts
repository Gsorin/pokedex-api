import {Injectable, Logger} from '@nestjs/common';
import {HttpService} from "@nestjs/axios";
import {lastValueFrom} from "rxjs";
import {Cron, CronExpression, SchedulerRegistry} from "@nestjs/schedule";

@Injectable()
export class PokemonsService{
  types = [];
  pokemons = [];
  moves = [];
  abilities = [];

  private readonly logger = new Logger(PokemonsService.name);
  constructor(private httpService: HttpService, private schedulerRegistry: SchedulerRegistry) {}

  @Cron("0 * */1 * * *")//"0 * */1 * * *")
  async triggerCronJobPokemons() {
    this.logger.log("Initialisation of all pokemons ...")
    await this.getAllPokemons()
    this.logger.log("✔ Pokemons are load");
  }

  async getPokemons() {
    return this.pokemons
  }

  async getAllPokemons(){

    await this.initAllAbilities();
    await this.initAllTypes();
    await this.initAllMoves();

    if(this.pokemons.length === 0) {
      for (let i = 1; i <= 898; i++) { //898 pokemons
        let response = await lastValueFrom(this.httpService.get(`https://pokeapi.co/api/v2/pokemon/${i}`).pipe())
        const species = await lastValueFrom(this.httpService.get(`https://pokeapi.co/api/v2/pokemon-species/${i}`).pipe())

        let json = {
          id: response.data.id,
          name: response.data.name,
          names : [
          {language: species.data.names.filter(language => language.language.name === "fr")[0].language.name, name : species.data.names.filter(language => language.language.name === "fr")[0].name},
          {language: species.data.names.filter(language => language.language.name === "en")[0].language.name, name : species.data.names.filter(language => language.language.name === "en")[0].name},
          {language: species.data.names.filter(language => language.language.name === "ja-Hrkt")[0].language.name, name : species.data.names.filter(language => language.language.name === "ja-Hrkt")[0].name},
          {language: species.data.names.filter(language => language.language.name === "de")[0].language.name, name : species.data.names.filter(language => language.language.name === "de")[0].name}
        ],
          abilities : response.data.abilities,
          moves : response.data.moves,
          stats: response.data.stats,
          types: response.data.types
        }

        json = this.changeAbilities(json)
        json = this.changeTypes(json)
        json = this.changeMoves(json)
        this.pokemons.push(json)
      }
    }
  }

  async initAllTypes(){
    if(this.types.length === 0) {
      for (let i = 1; i <= 18; i++) {
        const res = await lastValueFrom(this.httpService.get(`https://pokeapi.co/api/v2/type/${i}`).pipe())
        const type = {
          id: res.data.id,
          name: res.data.name,
          languages: [
            {language: res.data.names.filter(language => language.language.name === "fr")[0].language.name, name : res.data.names.filter(language => language.language.name === "fr")[0].name},
            {language: res.data.names.filter(language => language.language.name === "en")[0].language.name, name : res.data.names.filter(language => language.language.name === "en")[0].name},
            {language: res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].language.name, name : res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].name},
            {language: res.data.names.filter(language => language.language.name === "de")[0].language.name, name : res.data.names.filter(language => language.language.name === "de")[0].name}
          ],
        }
        this.types.push(type)
      }
    }
  }

  async initAllMoves(){
    if(this.moves.length === 0) {
      for (let i = 1; i <= 826; i++) {
        const res = await lastValueFrom(this.httpService.get(`https://pokeapi.co/api/v2/move/${i}`).pipe())

        const move = {
          id: res.data.id,
          name : res.data.name,
          stats : {
            pp: res.data.pp,
            power: res.data.power,
            priority: res.data.priority,
            accuracy: res.data.accuracy,
            damage_type: res.data.damage_class
          },
          languages: [
            {language: res.data.names.filter(language => language.language.name === "fr")[0].language.name, name : res.data.names.filter(language => language.language.name === "fr")[0].name},
            {language: res.data.names.filter(language => language.language.name === "en")[0].language.name, name : res.data.names.filter(language => language.language.name === "en")[0].name},
            {language: res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].language.name, name : res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].name},
            {language: res.data.names.filter(language => language.language.name === "de")[0].language.name, name : res.data.names.filter(language => language.language.name === "de")[0].name}
          ],
          type: res.data.type
        }

        this.moves.push(this.changeMovesTypes(move))
      }
    }
  }

  async initAllAbilities(){
    if(this.abilities.length === 0) {
      for (let i = 1; i <= 267; i++) {
        const res = await lastValueFrom(this.httpService.get(`https://pokeapi.co/api/v2/ability/${i}`).pipe())
        const ability = {
          id: res.data.id,
          name : res.data.name,
          languages: [
            {language: res.data.names.filter(language => language.language.name === "fr")[0].language.name, name : res.data.names.filter(language => language.language.name === "fr")[0].name},
            {language: res.data.names.filter(language => language.language.name === "en")[0].language.name, name : res.data.names.filter(language => language.language.name === "en")[0].name},
            {language: res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].language.name, name : res.data.names.filter(language => language.language.name === "ja-Hrkt")[0].name},
            {language: res.data.names.filter(language => language.language.name === "de")[0].language.name, name : res.data.names.filter(language => language.language.name === "de")[0].name}
          ],
        }
        this.abilities.push(ability)
      }
    }
  }

  changeAbilities(response) {

    const pokemonAbilities = []
    response.abilities.forEach(ability => {
      this.abilities.filter(e => {
        if(e.name === ability.ability.name){
          pokemonAbilities.push(
              {
                "ability": {
                  "name": ability.ability.name,
                },
                "is_hidden": ability.is_hidden,
                'languages' : e.languages
              }
          )

        }
      })
    })
    response.abilities = pokemonAbilities

    return response
  }

  changeTypes(response) {
    const pokemonTypes = []

    response.types.forEach(type => {
      this.types.forEach(e => {
        if(e.name === type.type.name){
          pokemonTypes.push(e)
        }
      })
    })
    response.types = pokemonTypes

    return response
  }

  changeMoves(response) {
    const pokemonMoves = []

    response.moves.forEach(move => {
      this.moves.forEach(e => {
        if(e.name === move.move.name){
          pokemonMoves.push(e)
        }
      })
    })
    response.moves = pokemonMoves

    return response
  }

  changeMovesTypes(response){
    let pokemonType = {};

      this.types.forEach(e => {
        if(e.name === response.type.name){
          pokemonType = e
        }
      })
    response.type = pokemonType

    return response
  }
}